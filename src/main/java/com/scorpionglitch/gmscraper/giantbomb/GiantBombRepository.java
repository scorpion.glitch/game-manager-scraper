package com.scorpionglitch.gmscraper.giantbomb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scorpionglitch.gmscraper.giantbomb.objects.GiantBombResponse;
import com.scorpionglitch.gmscraper.giantbomb.objects.GiantBombSearch;

public class GiantBombRepository {
	private static final String ACCEPT_VALUE = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
	private static final String USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0";
	
	private static final String API_KEY = System.getenv().get("GIANTBOMB_API");
	
	private static final String FILE_FORMAT = "json";
	
	private static final String GIANTBOMB_SEARCH_URL = "https://www.giantbomb.com/api/search/?api_key=" + API_KEY + "&format=" + FILE_FORMAT + "&query=%s";
	private static final String GIANTBOMB_SEARCH_GAME_URL = "https://www.giantbomb.com/api/search/?api_key=" + API_KEY + "&format=" + FILE_FORMAT + "&resources=game&query=%s";
	private static final String GIANTBOMB_RESOURCE_URL = "https://www.giantbomb.com/api/%s/%s/?api_key=" + API_KEY + "&format=" + FILE_FORMAT;
	
	private ObjectMapper objectMapper;
	
	public GiantBombRepository() {
		objectMapper = new ObjectMapper();
	}
	
	public String download(String urlAddress) throws IOException {
		URL queryURL = null;
		try {
			queryURL = new URL(urlAddress);
		} catch (MalformedURLException murle) {
			murle.printStackTrace();
			return null;
		}
		URLConnection queryUrlConnection = queryURL.openConnection();
		queryUrlConnection.setRequestProperty("Accept", ACCEPT_VALUE);
		queryUrlConnection.setRequestProperty("User-Agent", USER_AGENT_VALUE);
		queryUrlConnection.connect();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(queryUrlConnection.getInputStream()));
		
		StringBuilder builder = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return builder.toString();
	}

	public GiantBombResponse<GiantBombSearch[]> search(String query) {
		GiantBombResponse<GiantBombSearch[]> response = null;
		String download = null;
		try {
			query = URLEncoder.encode(query, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
		}
		try {
			download = download(String.format(GIANTBOMB_SEARCH_URL, query));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		if (download != null) {
			try {
				response = objectMapper.readValue(download, new TypeReference<GiantBombResponse<GiantBombSearch[]>>() {});
			} catch (JsonMappingException e) {
				System.out.println("Cannot map");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Literally cannot even");
				e.printStackTrace();
			}
		}
		return response;
	}
	
	public GiantBombResponse<GiantBombSearch[]> searchForGame(String query) {
		GiantBombResponse<GiantBombSearch[]> response = null;
		String download = null;
		try {
			query = URLEncoder.encode(query, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
		}
		try {
			download = download(String.format(GIANTBOMB_SEARCH_GAME_URL, query));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		if (download != null) {
			try {
				response = objectMapper.readValue(download, new TypeReference<GiantBombResponse<GiantBombSearch[]>>() {});
			} catch (JsonMappingException e) {
				System.out.println("Cannot map");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Literally cannot even");
				e.printStackTrace();
			}
		}
		return response;
	}
	
	public <T> GiantBombResponse<T> get(String resource, String guid, Class<T> returnClass) {
		GiantBombResponse<T> response = null;
		String download = null;
		try {
			download = download(String.format(GIANTBOMB_RESOURCE_URL, resource, guid));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		if (download != null) {
			try {
				response = objectMapper.readValue(download, new TypeReference<GiantBombResponse<T>>() {});
			} catch (JsonMappingException e) {
				System.out.println("Cannot map");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Literally cannot even");
				e.printStackTrace();
			}
		}
		return response;
	}
}
