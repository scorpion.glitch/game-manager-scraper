package com.scorpionglitch.gmscraper.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scorpionglitch.gmscraper.giantbomb.GiantBombRepository;
import com.scorpionglitch.gmscraper.giantbomb.objects.GiantBombGame;
import com.scorpionglitch.gmscraper.giantbomb.objects.GiantBombPlatform;
import com.scorpionglitch.gmscraper.giantbomb.objects.GiantBombRelease;
import com.scorpionglitch.gmscraper.giantbomb.objects.GiantBombResponse;
import com.scorpionglitch.gmscraper.giantbomb.objects.GiantBombSearch;

@RestController
@RequestMapping("/api/scraper/")
public class GMScraperController {
	private GiantBombRepository giantBombRepository;
	
	public GMScraperController() {
		giantBombRepository = new GiantBombRepository();
	}
	
	@GetMapping("search/{query}")
	public ResponseEntity<GiantBombResponse<GiantBombSearch[]>> search(@PathVariable String query) {
		return ResponseEntity.ok(giantBombRepository.search(query));
	}
	
	@GetMapping("searchgame/{query}")
	public ResponseEntity<GiantBombResponse<GiantBombSearch[]>> searchForGame(@PathVariable String query) {
		return ResponseEntity.ok(giantBombRepository.searchForGame(query));
	}
	
	@GetMapping("game/{guid}")
	public ResponseEntity<GiantBombResponse<GiantBombGame>> getGame(@PathVariable String guid) {
		return ResponseEntity.ok(giantBombRepository.get("game", guid, GiantBombGame.class));
	}
	
	@GetMapping("release/{guid}")
	public ResponseEntity<GiantBombResponse<GiantBombRelease>> getRelease(@PathVariable String guid) {
		return ResponseEntity.ok(giantBombRepository.get("release", guid, GiantBombRelease.class));
	}
	
	@GetMapping("platform/{guid}")
	public ResponseEntity<GiantBombResponse<GiantBombPlatform>> getPlatform(@PathVariable String guid) {
		return ResponseEntity.ok(giantBombRepository.get("platform", guid, GiantBombPlatform.class));
	}
}
